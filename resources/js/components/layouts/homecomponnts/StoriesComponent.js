import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {ClipLoader} from "react-spinners";
import moment from "moment";
function StoriesComponent(props) {
    const [story,setPstories] = useState();

    const renderStories = async () => {
       await axios.get('/api/getstoies').then(res=>{
          // console.log(res);
          if (res.status === 200){
              console.log(res.data);
              setPstories(res.data);
          }
      })
    }
    useEffect(()=>{
        renderStories();
        },[]
    )

    return (
        <div className="container">
            <div className="row justify-content-between">
                <div className="col-md-8">
                    <h5 className="font-weight-bold spanborder"><span>All Stories</span></h5>

                    {
                        story? story.map((item,i)=>{
                            return(
                                <div className="mb-3 d-flex justify-content-between" key={i}>
                                    <div className="pr-3">
                                        <h2 className="mb-1 h4 font-weight-bold">
                                            <Link to={`/blog/${item.id}`}>{item.title}</Link>
                                        </h2>
                                        <p>
                                            {item.description}
                                        </p>
                                        <div className="card-text text-muted small">
                                            Jake Bittle in SCIENCE
                                        </div>
                                        <small className="text-muted">{ moment(item.created_at).format("YYYY-MM-DD HH:mm:ss")}</small>
                                    </div>
                                    <img height="120" src={`/uploads/blog/${item.image}`} />
                                </div>
                            )
                        }) : (
                            <>
                                <ClipLoader speedMultiplier={1.6} color={"red"} />
                            </>
                        )
                    }
                </div>
                <div className="col-md-4 pl-4">
                    <h5 className="font-weight-bold spanborder"><span>Popular</span></h5>
                    <ol className="list-featured">
                        <li>
				<span>
				<h6 className="font-weight-bold">
				<a href="./article.html" className="text-dark">Did Supernovae Kill Off Large Ocean Animals?</a>
				</h6>
				<p className="text-muted">
					Jake Bittle in SCIENCE
				</p>
				</span>
                        </li>
                        <li>
				<span>
				<h6 className="font-weight-bold">
				<a href="./article.html" className="text-dark">Humans Reversing Climate Clock: 50 Million Years</a>
				</h6>
				<p className="text-muted">
					Jake Bittle in SCIENCE
				</p>
				</span>
                        </li>
                        <li>
				<span>
				<h6 className="font-weight-bold">
				<a href="./article.html" className="text-dark">Unprecedented Views of the Birth of Planets</a>
				</h6>
				<p className="text-muted">
					Jake Bittle in SCIENCE
				</p>
				</span>
                        </li>
                        <li>
				<span>
				<h6 className="font-weight-bold">
				<a href="./article.html" className="text-dark">Effective New Target for Mood-Boosting Brain Stimulation Found</a>
				</h6>
				<p className="text-muted">
					Jake Bittle in SCIENCE
				</p>
				</span>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    );
}

export default StoriesComponent;
