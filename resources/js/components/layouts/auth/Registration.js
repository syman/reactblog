import React, {useState} from 'react';
import {Link} from "react-router-dom";
import {useNavigate} from "react-router";
import swal from "sweetalert";

const Registration = () => {
    const [userInfo,setUserInfo] = useState({
        name:'',
        email:'',
        password:'',
        password_confirmation:'',
        errorlist:[]
    });
    var navigate = useNavigate();
    const inputHandle = (e) => {
        e.preventDefault();
        let name = e.target.name;
        let value = e.target.value;
        setUserInfo({...userInfo,[name]:value})
    }
    const fornHangle = (e) => {
      e.preventDefault();
      axios.post('/api/registration',userInfo).then(res=>{
          if(res.data.status === 200){
              console.log(res);
              localStorage.setItem('auth_token',res.data.token);
              localStorage.setItem('auth_email',res.data.email);
              swal('success',res.data.message,'success')
              navigate('/blogs');
              e.target.reset();
          }
          else {
              setUserInfo({...userInfo,['errorlist']:res.data})
              console.log(userInfo);
          }
      })
    }
    return (
        <div id="login">
            <div className="container">
                <div id="login-row" className="row justify-content-center align-items-center">
                    <div id="login-column" className="col-md-6">
                        <div id="login-box" className="col-md-12">
                            <form id="login-form" className="form" onSubmit={fornHangle}>
                                <h3 className="text-center text-info">Registration</h3>
                                <div className="form-group">
                                    <label htmlFor="username" className="text-info">Username:</label><br />
                                    <input autoComplete="off" type="text" onChange={inputHandle} name="name" id="username" className="form-control" />
                                    <span className="text-danger">{userInfo.errorlist.name}</span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email" className="text-info">Email:</label><br />
                                    <input autoComplete="off" type="email" onChange={inputHandle} name="email" id="email" className="form-control" />
                                    <span className="text-danger">{userInfo.errorlist.email}</span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password" className="text-info">Password:</label><br />
                                    <input autoComplete="off" type="password" onChange={inputHandle} name="password" id="password" className="form-control" />
                                    <span className="text-danger">{userInfo.errorlist.password}</span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password" className="text-info">Confirm Password:</label><br />
                                    <input autoComplete="off" type="password" onChange={inputHandle} name="password_confirmation" id="confirmed" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <input type="submit" name="submit" className="btn btn-info btn-md" value="Register" />
                                </div>
                                <div id="register-link" className="text-right">
                                    <Link to="/user/login">Login here</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Registration;
