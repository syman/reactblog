import React, {useState} from 'react';
import {Link} from "react-router-dom";
// import '../../../../css/login.css';
import swal from "sweetalert";
import {useNavigate} from "react-router";
const Login = () => {
    const navigate = useNavigate();
    const [userInfo,setUserInfo] = useState({
        name:'',
        email:'',
        message:'',
        errorlist:''
    });
    const hangleInput = (e) => {
        e.persist();
        setUserInfo({...userInfo,[e.target.name]:e.target.value});
    }

    const loginSubmit = (e) => {
        e.preventDefault();
        axios.post('/api/login',userInfo).then(res =>{
            if(res.data.status === 200){
                localStorage.setItem('auth_token',res.data.token);
                localStorage.setItem('auth_email',res.data.email);
                swal('success',res.data.message,'success');
                navigate('/blogs');
            }
            console.log(res);
        })
    }
    return (
        <div id="login">
            <div className="container">
                <div id="login-row" className="row justify-content-center align-items-center">
                    <div id="login-column" className="col-md-6">
                        <div id="login-box" className="col-md-12">
                            <form id="login-form" className="form" onSubmit={loginSubmit} >
                                <h3 className="text-center text-info">Login</h3>
                                <div className="form-group">
                                    <label htmlFor="email" className="text-info">Email:</label><br />
                                    <input onChange={hangleInput} type="email" name="email" id="email" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password" className="text-info">Password:</label><br />
                                    <input onChange={hangleInput} type="password" name="password" id="password" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <input type="submit" name="submit" className="btn btn-info btn-md" value="Login" />
                                </div>
                                <div id="register-link" className="text-right">
                                    <Link to="/user/registration">Register here</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
