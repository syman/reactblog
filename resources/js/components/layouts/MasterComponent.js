import React from 'react';
import ReactDOM from "react-dom";
import Nav from "./globalcomponents/Nav";
import Footer from "./globalcomponents/Footer";
import HomeComponent from "./pages/HomeComponent";
// import {Route, Link, Routes, BrowserRouter as Router} from "react-router-dom";
import { BrowserRouter as Router, Route, Link,Routes } from "react-router-dom";
import AboutComponent from "./pages/AboutComponent";
import ContactComponent from "./pages/ContactComponent";
import CreateBlogComponent from "./pages/CreateBlogComponent";
import BlogsComponent from "./pages/BlogsComponent";
import ViewBlogComponent from "./pages/ViewBlogComponent";
import NotFound from "./pages/NotFound";
import EditBlogComponent from "./pages/EditBlogComponent";
import Registration from "./auth/Registration";
import Login from "./auth/Login";
function MasterComponent(props) {
    return (
        <>
            <Router>
                <Nav/>
                <Routes>
                    <Route exact path='/' element={<HomeComponent/>}/>
                    <Route path='/about' element={<AboutComponent/>}/>
                    <Route path='/contact' element={<ContactComponent/>}/>
                    <Route path='/blog-create' element={<CreateBlogComponent/>}/>
                    <Route path='/blogs' element={<BlogsComponent/>}/>
                    <Route path='/blog/:id' element={<ViewBlogComponent/>}/>
                    <Route path='/blog/edit/:id' element={<EditBlogComponent/>}/>
                    <Route path='/user/registration' element={<Registration/>}/>
                    <Route path='/user/login' element={<Login/>}/>
                    <Route path='*' element={<NotFound/>}/>
                </Routes>
                <Footer />
            </Router>
        </>
    );
}

export default MasterComponent;
if (document.getElementById('app')) {
    ReactDOM.render(<MasterComponent />, document.getElementById('app'));
}
