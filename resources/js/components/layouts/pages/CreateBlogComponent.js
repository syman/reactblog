import React, {useState} from 'react';
import swal from 'sweetalert';
function CreateBlogComponent(props) {
    const [state, setState] = useState({
        title: '',
        description: '',
        tags:'',
        image:''
    });

    const inpurHandaler = (e) => {
        var name = e.target.name;
        var value = e.target.value;
        setState({...state,[name]:value});
    }
    const formsubmit = (e) => {
      e.preventDefault();
      axios.post('/api/blogs',state).then(res => {
          if (res.status === 200){
              console.log(res);
              setState({...state,
                  title: '',
                  description: '',
                  tags:'',
                  image:[]
              });
              console.log('Post created');
              e.target.reset();
              swal('success','Blog created successfully','success')
          }
      });
    }
    const fileHandle = (e) => {
        const files = e.target.files;
        createImage(files[0])
    }


    function createImage(file) {
        let reader = new FileReader();
        reader.onload = (e) => {
            setState({...state,['image']:e.target.result})
        };
        reader.readAsDataURL(file);
    }
    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="card-body">
                            <form onSubmit={formsubmit} encType="multipart/form-data">
                                <div className="form-outline mb-4">
                                    <input onChange={inpurHandaler} type="text" name="title" className="form-control" />
                                    <label className="form-label" htmlFor="form4Example1">Title</label>
                                </div>
                                <div className="form-outline mb-4">
                                    <textarea onChange={inpurHandaler} name="description" className="form-control" rows="4"></textarea>
                                    <label className="form-label" htmlFor="form4Example3">Description</label>
                                </div>
                                <div className="form-outline mb-4">
                                    <input onChange={inpurHandaler} type="text" name="tags" className="form-control"/>
                                    <label className="form-label" htmlFor="form4Example1">Tags</label>
                                </div>
                                <div className="form-outline mb-4">
                                    <input onChange={fileHandle} type="file" name="image" className="file-input"/>
                                    <label className="form-label" htmlFor="form4Example1">Image</label>
                                </div>
                                <button type="submit" className="btn btn-primary btn-block mb-4">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreateBlogComponent;
