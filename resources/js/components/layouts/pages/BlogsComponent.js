import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import swal from 'sweetalert';
const BlogsComponent = () => {
    const [blogs, setBlogs] = useState();
    const fetchData = async () => {
      await axios.get('/api/blogs',{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('auth_token')}`
            }
        }).then(res =>{
          // console.log(res.data.blogs);
          setBlogs(res.data.blogs);
      })
    }

    useEffect(() => {
        fetchData();
    },['abc']);
    var blog = '';
    blog = blogs ? blogs.map((item,i) => {
        return(
            <tr key={i}>
                <td><Link to={`/blog/${item.id}`}>{item.title}</Link></td>
                <td>{item.tags}</td>
                <td>
                    <Link to={`/blog/edit/${item.id}`}>Edit</Link>
                    <button onClick={()=>{
                        axios.delete(`/api/blogs/${item.id}`).then(res => {
                            fetchData();
                        });
                        console.warn(item.id);
                    }} className="btn btn-danger" >Delete</button>
                </td>
            </tr>
        )
    }) : <p>loading</p>;
    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <table className="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Title</th>
                                <th>Tags</th>
                                <th>Action</th>
                            </tr>
                            {blog}
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    );
};

export default BlogsComponent;
