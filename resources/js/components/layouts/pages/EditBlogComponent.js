import React, {useEffect, useState} from 'react';
import {useNavigate, useParams} from "react-router";
import swal from 'sweetalert';

const EditBlogComponent = () => {
    var navigate = useNavigate();
    const [blog,setBlog] = useState({
        title: '',
        description: '',
        tags:''
    });
    const param = useParams();
    const inpurHandaler = (e) =>{
        var name = e.target.name;
        var value = e.target.value;
        setBlog({...blog,[name]:value});
    }
    const formUpdate = (e) =>{
        e.preventDefault();
        axios.put(`/api/blogs/${param.id}`,blog).then(res =>{
            if (res.status === 200){
                console.log('done');
                swal('success','Blog updated successfully','success')
                navigate('/blogs');

                }
            }
        )
    }
    const fileHandle = () =>{

    }

    useEffect(async ()=>{
        await axios.get(`/api/blogs/${param.id}`).then(res =>{
            setBlog(res.data);
        })
    },[]);
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <form onSubmit={formUpdate} encType="multipart/form-data">
                                    <div className="form-outline mb-4">
                                        <input onChange={inpurHandaler} type="text" name="title" value={blog.title} className="form-control" />
                                        <label className="form-label" htmlFor="form4Example1">Title</label>
                                    </div>
                                    <div className="form-outline mb-4">
                                        <textarea onChange={inpurHandaler} name="description" className="form-control" rows="4" value={blog.description}></textarea>
                                        <label className="form-label" htmlFor="form4Example3">Description</label>
                                    </div>
                                    <div className="form-outline mb-4">
                                        <input onChange={inpurHandaler} type="text" name="tags" value={blog.tags} className="form-control"/>
                                        <label className="form-label" htmlFor="form4Example1">Tags</label>
                                    </div>
                                    <img src={`/uploads/blog/${blog.image}`} />
                                    <div className="form-outline mb-4">
                                        <input onChange={fileHandle} type="file" name="image" className="file-input"/>
                                        <label className="form-label" htmlFor="form4Example1">Image</label>
                                    </div>
                                    <button type="submit" className="btn btn-primary btn-block mb-4">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EditBlogComponent;
