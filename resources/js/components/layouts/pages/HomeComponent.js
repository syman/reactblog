import React from 'react';
import Hero from "../homecomponnts/Hero";
import ArticalsComponent from "../homecomponnts/ArticalsComponent";
import StoriesComponent from "../homecomponnts/StoriesComponent";

function HomeComponent(props) {
    return (
        <div>
            <Hero/>
            <ArticalsComponent/>
            <StoriesComponent/>
        </div>
    );
}

export default HomeComponent;
