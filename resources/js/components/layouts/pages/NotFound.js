import React from 'react';

const NotFound = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="d-flex justify-content-center align-items-center">
                        <h1 className="align-content-center" >404</h1>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default NotFound;
