import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link,Routes } from "react-router-dom";
import {useNavigate} from "react-router";
import swal from "sweetalert";
function Nav() {
    const navigate = useNavigate();
    const logout = (e) => {
      e.preventDefault();
      axios.post('/api/logout','',{
          headers: {
              'Authorization': `Bearer ${localStorage.getItem('auth_token')}`
          }
      }).then(res=>{
          if (res.data.status === 200){
              localStorage.removeItem('auth_token');
              localStorage.removeItem('auth_email');
              swal('success',res.data.message,'success');
              navigate('/');
          }
      })
    }
    return (
        <nav className="topnav navbar navbar-expand-lg navbar-light bg-white fixed-top">
            <div className="container">
                <a className="navbar-brand" href="/"><strong>Mundana</strong></a>
                <button className="navbar-toggler collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="navbar-collapse collapse" id="navbarColor02">
                    <ul className="navbar-nav mr-auto d-flex align-items-center">
                        <li className="nav-item">
                            <Link className="nav-link" to='/'>Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to='/about'>About</Link>
                        </li>
                        {localStorage.getItem('auth_token') ? (
                            <>
                                <li className="nav-item">
                                    <Link className="nav-link" to='/blog-create'>Create blog</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to='/blogs'>Blogs</Link>
                                </li>
                            </>
                        ): ''}
                        <li className="nav-item">
                            <Link className="nav-link" to='/contact'>Contact</Link>
                        </li>
                    </ul>
                    {localStorage.getItem('auth_token') ? (
                        <ul className="navbar-nav ml-auto d-flex align-items-center">
                            <li className="nav-item highlight">
                                <button className="nav-link btn" onClick={logout}>Logout</button>
                            </li>
                        </ul>
                    ) : (
                        <ul className="navbar-nav ml-auto d-flex align-items-center">
                            <li className="nav-item highlight">
                                <Link className="nav-link" to='/user/login'>Login</Link>
                            </li>
                        </ul>
                    )}

                </div>
            </div>
        </nav>
    );
}

export default Nav;
