<?php

namespace App\Http\Controllers;

use App\Models\Blogs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $blogs = Blogs::latest()->get();
        return response()->json(['blogs' => $blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->image) {
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            Image::make($request->get('image'))->save(public_path('uploads/blog/').$name);
            $data['image'] = $name;
        }
        Blogs::create($data);
        return response('success',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blogs  $blogs
     * @return Response
     */
    public function show($id)
    {
        $blog = Blogs::find($id);
        $nextblog = Blogs::inRandomOrder($id)->first();
//        return $nextblog;
        return response($blog,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blogs  $blogs
     * @return Response
     */
    public function edit(Blogs $blogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blogs  $blogs
     * @return Response
     */
    public function update(Request $request, $blog)
    {
        $blogs = Blogs::find($blog);
        $blogs->update($request->all());
        return response('success',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blogs  $blogs
     * @return Response
     */
    public function destroy($blog)
    {
        $blogs = Blogs::find($blog);
        $blogs->delete();
        return response('success',200);
    }
}
