<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function registration(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255','min:3'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        $token = $user->createToken($user->email, ['_token'])->plainTextToken;

        return response()->json([
            'status' => 200,
            'email' => $user->email,
            'token'=> $token,
            'message' => 'Registration successful'
        ]);
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user = User::where('email', $request->email)->first();
        if (! $user || ! Hash::check($request->password, $user->password)) {
            return response()->json([
                'status' => 401,
                'message' => 'The provided credentials are incorrect.'
            ]);
        }
        else {
            $token = $user->createToken($user->email, ['_token'])->plainTextToken;

            return response()->json([
                'status' => 200,
                'email' => $user->email,
                'token'=> $token,
                'message' => 'Login successful'
            ]);
        }
    }

    public function logout() {
        auth()->user()->tokens()->delete();
        return response()->json([
            'status' => 200,
            'message' => 'You are logged out'
        ]);
    }
}
